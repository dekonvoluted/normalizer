#! /usr/bin/env ruby

# Normalizer
# Copyright (C) 2019  Karthik Periagaram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Add replay-gain/volume normalization tags to music files

require "optparse"
require "pathname"

libnormalize = File.expand_path "../lib", __FILE__
$LOAD_PATH.unshift libnormalize unless $LOAD_PATH.include? libnormalize

require "normalize"
require "version"

if __FILE__ == $0

  # GPL disclaimer
  disclaimer = <<~EOGPL
    Normalizer
    Copyright (C) 2019  Karthik Periagaram
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions. See COPYING for more details.
  EOGPL

  # Parse command line options
  options = Hash.new
  OptionParser.new do |opts|
    opts.banner = "Usage: #{$0} [OPTIONS] [DIR]..."

    opts.on("--version", "Print the version") do
      puts "Normalizer version #{Normalizer::VERSION}"
      exit
    end

    opts.on_tail("-h", "--help", "Print this help message") do
      # Print out the GPL disclaimer
      puts disclaimer
      puts
      puts opts
      exit
    end
  end.parse! into: options

  # Assume current directory as sole argument if none are provided
  ARGV.push "./" if ARGV.empty?

  # Ensure all inputs are valid directories
  ARGV.each do |input|
    path = Pathname.new input
    raise Normalizer::ArgumentError, "Input is not a valid directory: #{input}" unless path.directory?

    Normalizer.normalize path
  end

end

