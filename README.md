# Normalizer

Add volume normalization tags to music files.

## Usage

The input to `normalize` is zero or more directory paths.
It will disregard any paths that do not represent directories.

If the directory contains subdirectories, it will recursively descend into those subdirectories.
At each level, it collects all music files and verifies that they have a volume normalization tag.
If any of the files are missing their normalization tag, it will process them together and add normalization tags to all of them.

```
$ normalize [OPTIONS] [DIR]...
```

