# Normalizer
# Copyright (C) 2019  Karthik Periagaram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module METAFLAC

  BINARY = "/bin/metaflac"

  def self.normalize(flacpaths, force=false)
    # Normalize multiple FLAC files together as an album

    # Leave if all files have replaygain tags (unless forced)
    unless force
      return 0 if flacpaths.all? { |flacpath| flacpath.has_replaygain_tags }
    end

    # Add replay gain tags
    cmd = Array.new
    cmd.push BINARY
    cmd.push "--preserve-modtime"
    cmd.push "--add-replay-gain"
    cmd.concat flacpaths.map { |flacpath| flacpath.to_s }

    IO.popen(cmd.join " ")
    $?.exitstatus
  end

  def normalize(force=false)
    # Normalize self as a track

    # Leave if already normalized (unless forced)
    unless force
      return 0 if self.has_replaygain_tags
    end

    cmd = Array.new
    cmd.push BINARY
    cmd.push "--preserve-modtime"
    cmd.push "--add-replay-gain"
    cmd.push self.to_s

    IO.popen(cmd.join " ")
    $?.exitstatus
  end

  def has_tag(tag)
    # Verify the presence of a tag in the FLAC metadata
    # Note that the file name does not need to be sanitized for the shell
    cmd = Array.new
    cmd.push BINARY
    cmd.push "--show-tag=#{tag}"
    cmd.push self.to_s

    output = String.new
    IO.popen(cmd) { |p| output = p.read }

    if $?.exitstatus == 0
      not output.empty?
    else
      false
    end
  end

  def has_replaygain_tags()
    # Verify that the file has the five replay gain tags

    replaygain_tags = Array.new
    replaygain_tags.push "REPLAYGAIN_REFERENCE_LOUDNESS"
    replaygain_tags.push "REPLAYGAIN_TRACK_GAIN"
    replaygain_tags.push "REPLAYGAIN_TRACK_PEAK"
    replaygain_tags.push "REPLAYGAIN_ALBUM_GAIN"
    replaygain_tags.push "REPLAYGAIN_ALBUM_PEAK"

    replaygain_tags.all? { |tag| self.has_tag tag }
  end

  def get_tag(tag, default=nil)
    # Fetch the value set for the tag
    # Optionally, return default value if tag is not set

    cmd = Array.new
    cmd.push BINARY
    cmd.push "--show-tag=#{tag}"
    cmd.push self.to_s

    output = String.new
    IO.popen(cmd) { |p| output = p.read }

    if $?.exitstatus == 0
      key, value = output.split "="
      value
    else
      default
    end
  end

  #list all tags:
  #  metaflac --list --block-type=VORBIS_COMMENT file

end
