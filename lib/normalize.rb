# Normalizer
# Copyright (C) 2019  Karthik Periagaram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "flacpath"

module Normalizer

  def self.normalize(path)

    # Return early if directory is empty
    return if path.empty?

    # Normalize all FLAC files
    paths = path.children.select { |child| child.extname.downcase == ".flac" }
    METAFLAC.normalize paths.map { |path| FLACPath.new path }

    # Recursively process subdirectories
    path.children.select(&:directory?).each do |subdir|
      Normalizer.normalize subdir
    end
  end

end

